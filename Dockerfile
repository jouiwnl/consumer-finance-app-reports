FROM node:18-alpine

# make the 'app' folder the current working directory
WORKDIR /app

# Copy configuration files
COPY tsconfig*.json ./
COPY package*.json ./

# install project dependencies
RUN npm install

# Copy application sources (.ts, .tsx, js)
COPY ./src ./src/
COPY ./prisma ./prisma/

RUN npx prisma generate

# Build application (produces dist/ folder)
RUN npm run build

EXPOSE 3000

# Start application
CMD [ "node", "build/index.js" ]
